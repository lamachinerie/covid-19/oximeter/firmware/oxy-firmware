/*                                                      
 _____             _____ _                             
|     |_ _ _ _ ___|   __|_|___ _____ _ _ _ ___ ___ ___ 
|  |  |_'_| | |___|   __| |  _|     | | | | .'|  _| -_|
|_____|_,_|_  |   |__|  |_|_| |_|_|_|_____|__,|_| |___|
          |___|                                        

  desc : Firmware for opensource oxymeter
  author : JulesTopart
  organization : LaMachinerie, Etoele, Electrolab
  license: MIT

*/

#include "core/oxyMain.h"

void setup() {  
  Serial.begin(9600);
  Serial.println("Initializing...");
  //setupSD();
  initSensor();
  delay(1000);
  setupConnection();
  //connectEthernet();
}

void loop() {
  runMeasurement();
}

