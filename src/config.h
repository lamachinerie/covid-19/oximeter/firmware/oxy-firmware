
#define __DEBUG__

//#define NELCOR
#define MAXIM


#ifdef MAXIM

#define LED_OFF     0x00
#define LED_LOW     0x0A
#define LED_MEDIUM  0x7F
#define LED_HIGH    0xFF

#define LED_AMPLITUDE LED_OFF


#endif