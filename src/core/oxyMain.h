#ifndef OXYMAIN_H
#define OXYMAIN_H

#include <Arduino.h>

#include "config.h"

#ifdef NELCOR
//#include "nelcor/afe44xx.h"
#elif defined(MAXIM)
#include <Wire.h>
#include "maxim/MAX30105.h"           //MAX3010x library
#include "core/spo2.h"
#include "core/irCurve.h"
#include "data/comSD.h"
#include "data/comEthernet.h"

extern MAX30105 oxymeterSensor;
#endif

void    initSensor();
void    gatherSamples();
int     getSpo2ToSend();
void    runMeasurement();


float   thermometer();

#endif