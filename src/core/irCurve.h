#ifndef CORE_IRCURVE_H
#define CORE_IRCURVE_H

#include <Arduino.h>

#define IRSAMPLE_SIZE 50
typedef struct irSample
{
    int values[IRSAMPLE_SIZE];
    int time[IRSAMPLE_SIZE];
    int count = 0;
    bool valid = false;

    bool push(int i, long t)
    {
        if (count <= IRSAMPLE_SIZE - 1)
        {
            values[count++] = i;
            time[count] = t;
            return true;
        }else return false;
    }

    bool complete() { return count == IRSAMPLE_SIZE; }

} irSample;

irSample    gatheriIRSample();
String      IRSampleToJSON(irSample);

#endif