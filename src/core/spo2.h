#ifndef CORE_SPO2_H
#define CORE_SPO2_H

#include <Arduino.h>

#define SAMPLE_SIZE 4
typedef struct spo2Sample{
  int values[SAMPLE_SIZE];
  int count = 0;
  bool valid = false;

  void push(int i) { if(count <= SAMPLE_SIZE - 1) values[count++] = i; }
  bool complete(){ return count == SAMPLE_SIZE;}
  void setInvalid(){valid = false;}

  int moyenne(){
    int result = 0;
    for(int i = 0; i < SAMPLE_SIZE; i++) result += values[i];
    return result/SAMPLE_SIZE;
  }

}spo2Sample;

void setSpO2SignalRange();

spo2Sample gatherSpO2Sample();

#endif