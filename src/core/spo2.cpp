#include "core/spo2.h"
#include "core/oxyMain.h"

#include "maxim/heartRate.h"          //Heart rate calculating algorithm
#include "maxim/spo2_algorithm.h"

#define MAX_BRIGHTNESS 255
#define BUFSIZE 20

#define TIMEOUT 10000

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
//Arduino Uno doesn't have enough SRAM to store 100 samples of IR led data and red led data in 32-bit format
//To solve this problem, 16-bit MSB of the sampled data will be truncated. Samples become 16-bit data.
unsigned short irBuffer[BUFSIZE]; //infrared LED sensor data
unsigned short redBuffer[BUFSIZE];  //red LED sensor data
#else
unsigned int irBuffer[BUFSIZE]; //infrared LED sensor data
unsigned int redBuffer[BUFSIZE];  //red LED sensor data
#endif

int bufferLength; //data length
int spo2; //SPO2 value
signed char  validSPO2; //indicator to show if the SPO2 calculation is valid
int heartRate; //heart rate value
signed char validHeartRate; //indicator to show if the heart rate calculation is valid

void setSpO2SignalRange (){
  bufferLength = BUFSIZE; //buffer length of 100 stores 4 seconds of samples running at 25sps

  Serial.println("Calculating signal range...");
  //read the first 100 samples, and determine the signal range
  for (byte i = 0 ; i < bufferLength ; i++)
  {
    while (oxymeterSensor.available() == false) //do we have new data?
      oxymeterSensor.check(); //Check the sensor for new data

    redBuffer[i] = oxymeterSensor.getRed();
    irBuffer[i] = oxymeterSensor.getIR();
    oxymeterSensor.nextSample(); //We're finished with this sample so move to next sample
  }

  //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
  maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
}

spo2Sample gatherSpO2Sample(){
  bufferLength = BUFSIZE; //buffer length of 100 stores 4 seconds of samples running at 25sps

  Serial.println("Calculating Heartrate & SpO2...");

  spo2Sample samp;
  //Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
  long time = millis();
  while (!samp.complete() && (millis() - time < TIMEOUT))
  {
    //dumping the first 25 sets of samples in the memory and shift the last 25 sets of samples to the top
    for (byte i = bufferLength/2; i < bufferLength; i++)
    {
      redBuffer[i - bufferLength/2] = redBuffer[i];
      irBuffer[i - bufferLength/2] = irBuffer[i];
    }

    //take 25 sets of samples before calculating the heart rate.
    for (byte i = bufferLength/2; i < bufferLength; i++)
    {
      while (oxymeterSensor.available() == false) //do we have new data?
        oxymeterSensor.check(); //Check the sensor for new data

      redBuffer[i] = oxymeterSensor.getRed();
      irBuffer[i] = oxymeterSensor.getIR();
      oxymeterSensor.nextSample(); //We're finished with this sample so move to next sample

    }

    //After gathering 25 new samples recalculate HR and SP02
    maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);

    if(validSPO2 == 1) samp.push(spo2);
    if(validSPO2 == 1){ Serial.print(spo2); Serial.print("|");}
  }
  Serial.println();
  if(samp.complete()) samp.valid = true;

  return samp;
}