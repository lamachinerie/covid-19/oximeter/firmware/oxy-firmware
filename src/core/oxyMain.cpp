#include "oxyMain.h"

#ifdef NELCOR
//#include "nelcor/afe44xx.h"
#elif defined(MAXIM)
#include <Wire.h>
#include "maxim/MAX30105.h"           //MAX3010x library

MAX30105 oxymeterSensor;
#endif

String IRDataToSend = "";
int spo2ToSend = -1;

void initSensor(){
  // Initialize sensor
  if (!oxymeterSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Serial.println("MAX30105 was not found. Please check wiring/power. ");
    while (1);
  }

  //Setup to sense a nice looking saw tooth on the plotter
  byte ledBrightness = 60;   //Options: 0=0x00=Off to x=0xFF=255=50mA
  byte sampleAverage = 4;    //Options: 1, 2, 4, 8, 16, 32
  byte ledMode       = 2;    //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  int sampleRate     = 100;  //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  int pulseWidth     = 411;  //Options: 69, 118, 215, 411
  int adcRange       = 4096; //Options: 2048, 4096, 8192, 16384

  oxymeterSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange); //Configure sensor with these settings
}

void gatherSamples(){
  Serial.println("######## New Sample ########");
  spo2Sample ss = gatherSpO2Sample();
  if(!ss.valid) Serial.println("Merci de reposer votre doigt");
  else{
    Serial.print("M : ");
    Serial.println(ss.moyenne(), DEC);
    spo2ToSend = ss.moyenne();
  }

  irSample IS = gatheriIRSample();
  IRDataToSend = IRSampleToJSON(IS);


  Serial.println("######## End Sample ########");
}

int getSpo2ToSend(){
  int result = spo2ToSend;
  spo2ToSend = -1;
  return result;
}

String getIRToSend(){
  String result = IRDataToSend;
  IRDataToSend = "";
  return result;
}


float thermometer(){
  return oxymeterSensor.readTemperature();
}

void runMeasurement(){
  gatherSamples();
  int spo2Tosend = getSpo2ToSend();
  if(spo2Tosend != -1) comHttpSpO2(105, spo2Tosend);
  String jsonIR = getIRToSend();
  if(jsonIR != ""){
    comHttpIR(105,jsonIR);
  }
}