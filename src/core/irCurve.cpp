#include "oxyMain.h"
#include "irCurve.h"


irSample gatheriIRSample(){
    Serial.println("Recording IR...");
    long timer = millis();
    irSample data;
    while (!data.complete()){
        data.push(oxymeterSensor.getIR(), millis() - timer);
    }
    return data;
}

String IRSampleToJSON(irSample IS){
    String json = "{\"data\":[";
    char buffer[50];
    for (size_t i = 0; i < IS.count; i++){
        json += "[";
        json += itoa(IS.values[i],buffer,10);
        json += ",";
        json += itoa(IS.time[i],buffer,10);
        json += "]";
        if(i < IS.count - 1) json += ",";
    }
    json += "]}";
    return json;
    
}