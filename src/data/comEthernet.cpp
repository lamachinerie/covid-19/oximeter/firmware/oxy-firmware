#include <SPI.h>
#include <Ethernet.h>

#include "data/comEthernet.h"

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(192, 168, 0, 177);
IPAddress myDns(192, 168, 0, 1);


// initialize the library instance:
EthernetClient client;
//char server[] = "www.arduino.cc";  // also change the Host line in httpRequest()
IPAddress server(192,168,0,5);

void setupConnection(){
  Ethernet.init(10);  // Most Arduino shields
  
  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
    Serial.print("My IP address: ");
    Serial.println(Ethernet.localIP());
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);
}

void comHttpSpO2(int id, int spo2) {
  if (client.available()) {
    char c = client.read();
    Serial.write(c);
  }

  spO2httpRequest(id, spo2);


}


void comHttpIR(int id, String json) {
  if (client.available()) {
    char c = client.read();
    Serial.write(c);
  }


  IRhttpRequest(id, json);
}

// this method makes a HTTP connection to the server:
void spO2httpRequest(int id, int spo2) {
  char erreur = client.connect(server, 8080);

  if(erreur == 1) {
      // Pas d'erreur ? on continue !
      Serial.println("Connexion OK, envoi en cours...");

      // On construit l'en-tête de la requête
      client.print("GET /data/?id="); //attention, pas de saut de ligne !
      client.print(id);
      client.print("&spo2=");
      client.print(spo2);
      client.print("&millis=");
      client.print(millis());
      // On finit par le protocole
      client.println(" HTTP/1.1"); //ce coup-ci, saut de ligne pour valider !
      // on aurait alors :
      // "GET /enregistrer.php/?analog1=<valeur-de-A1>&analog2=<valeur-de-A2>&millis=<valeur-de-millis()> HTTP/1.1"
      client.println("Host: 192.168.0.5");
      client.println("Connection: close");
      client.println();

  } else {
    // La connexion a échoué :(
    // On ferme notre client
    client.stop();
    // On avertit l'utilisateur
    Serial.println("Echec de la connexion");
    switch(erreur) {
      case(-1):
        Serial.println("Time out");
        break;
      case(-2):
        Serial.println("Serveur invalide");
        break;
      case(-3):
        Serial.println("Tronque");
        break;
      case(-4):
        Serial.println("Reponse invalide");
        break;
    }
  }
}




void IRhttpRequest(int id, String json) {
  char erreur = client.connect(server, 8080);

  if(erreur == 1) {
      // Pas d'erreur ? on continue !
      Serial.println("Connexion OK, envoi IR en cours...");

      // On construit l'en-tête de la requête
      client.print("GET /data/?id="); //attention, pas de saut de ligne !
      client.print(id);
      client.print("&ir=");
      client.print(json);
      client.print("");
      // On finit par le protocole
      client.println(" HTTP/1.1"); //ce coup-ci, saut de ligne pour valider !
      // on aurait alors :
      // "GET /enregistrer.php/?analog1=<valeur-de-A1>&analog2=<valeur-de-A2>&millis=<valeur-de-millis()> HTTP/1.1"
      client.println("Host: 192.168.0.5");
      client.println("Connection: close");
      client.println();

  } else {
    // La connexion a échoué :(
    // On ferme notre client
    client.stop();
    // On avertit l'utilisateur
    Serial.println("Echec de la connexion");
    switch(erreur) {
      case(-1):
        Serial.println("Time out");
        break;
      case(-2):
        Serial.println("Serveur invalide");
        break;
      case(-3):
        Serial.println("Tronque");
        break;
      case(-4):
        Serial.println("Reponse invalide");
        break;
    }
  }
}